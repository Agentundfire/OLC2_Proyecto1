package olc2_proyecto1;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.lang.reflect.Field;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Componente {

    private final JPanel PADRE;
    private final String TAG;
    private Component BODY;

    private String ID, GRUPO, Texto, click, ruta, valor = null; //CHTML - CCSS
    
//    private String Texto, Letra, TamTex, Visible,
//            Borde, Opaque, ColorText, AutoRedim = null; // CCSS

    int Alto, Ancho;
    float Alineado;
    
    boolean[] Formato = new boolean[5];
    Color Fondo = Color.WHITE;

    public Componente(JPanel contenedor, String tag) {
        this.TAG = tag;
        this.PADRE = contenedor;
        
        this.Texto = "";
        this.Alto = 50;
        this.Ancho = 100;
        this.Alineado = 0.5f; //Component.CENTER_ALIGNMENT;
    }

//---------------------------------------------------------------------
//-------------------------    Metodos CHTML
//---------------------------------------------------------------------

    public static boolean pathExists(String url) {
        if(url == null) 
            return false;
        
        return new File(url).exists();
    }
    
    public boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    public static String trimSTR(String value) {
        if (value == null) {
            return "";
        }
        return value.substring(1, value.length() - 1);
    }
 
// * Original Author:  David Gilbert (for Object Refinery Limited);
    public static Color stringToColor(String value) {
        if (value == null) {
            return Color.BLACK;
        }
        
        value = trimSTR(value);
        try {
            // get color by hex or octal value
            return Color.decode(value);
        } catch (NumberFormatException nfe) {
            // if we can't decode lets try to get it by name
            try {
                // try to get a color by name using reflection
                Field f = Color.class.getField(value);

                return (Color) f.get(null);
            } catch (Exception ce) {
                // if we can't get any color return black
                return Color.BLACK;
            }
        }
    }

    public void addToPadre(Component awt){
        this.BODY = awt;
        this.PADRE.add(BODY);
    }
    
//---------------------------------------------------------------------
//-------------------------    Setters
//---------------------------------------------------------------------
       
    public void setAlto(String str) {
        str = trimSTR(str);
        if (isInteger(str)) {
            this.Alto = Integer.parseInt(str);
        }
    }
    
    public void setAncho(String str) {
        str = trimSTR(str);
        if (isInteger(str)) {
            this.Ancho = Integer.parseInt(str);
        }
    }
    
    public void setAlineado(String Alineado) {
        Alineado = trimSTR(Alineado);
        Alineado = Alineado.toLowerCase();
        switch (Alineado) {
            case "izquierda":
            case "izquierdo":
                this.Alineado = 0.0f;
                break;
            case "centro":
            case "centrado":
                this.Alineado = 0.5f;
                break;
            case "derecha":
            case "derecho":
                this.Alineado = 1.0f;
                break;
            default:
                System.out.println("\n\t ERROR en setAlineado");
        }
    }
    
    public void setID(String ID) {
        this.ID = trimSTR(ID);
    }

    public void setGRUPO(String GRUPO) {
        this.GRUPO = trimSTR(GRUPO);
    }
    
    public void setFondo(String value) {
        this.Fondo = stringToColor(value);
    }
    
    public void setClick(String click) {
        this.click = trimSTR(click);
    }
    
    public void setRuta(String ruta) {
        this.ruta = trimSTR(ruta);
    }

    public void setValor(String valor) {
        this.valor = trimSTR(valor);
    }

    public void setTexto(String Texto) {
        this.Texto = Texto;
    }
    
//---------------------------------------------------------------------
//-------------------------    Getters Personalizados
//---------------------------------------------------------------------

    public ImageIcon getImagen(){
        if(pathExists(this.ruta)){
            return new ImageIcon(this.ruta);
        } 
        if (pathExists(this.Texto)){
            return new ImageIcon(this.Texto);
        }
        return new ImageIcon("src/img/missing.png");
    }

    public int getNumero(){
        if(isInteger(this.Texto)){
            return Integer.parseInt(this.Texto);
        }
        
        return 0;
    }
    
    public String getValor() {
        if(valor != null){
            return valor;
        }
        return Texto;
    }
    
//---------------------------------------------------------------------
//-------------------------    Getters
//---------------------------------------------------------------------
    
    public JPanel getPadre() {
        return PADRE;
    }

    public String getTAG() {
        return TAG;
    }
    
    public String getID() {
        return ID;
    }

    public String getGRUPO() {
        return GRUPO;
    }

    public int getAlto() {
        return Alto;
    }

    public int getAncho() {
        return Ancho;
    }

    public float getAlineado() {
        return Alineado;
    }

    public boolean[] getFormato() {
        return Formato;
    }

    public Color getFondo() {
        return Fondo;
    }

    public String getClick() {
        return click;
    }

    public String getRuta() {
        return ruta;
    }

    public String getTexto() {
        return Texto;
    }

    public Component getBody() {
        return BODY;
    }

}
