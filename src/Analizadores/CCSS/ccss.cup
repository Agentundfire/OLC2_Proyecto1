/*--------------- 1ra Area: Codigo de Usuario -----------------------*/
//-------> importaciones, paquetes
package Analizadores.CCSS;

import olc2_proyecto1.Nodo;
import olc2_proyecto1.TError;
import java_cup.runtime.Symbol;
import java.util.LinkedList;

//------> Codigo para el parser,variables, metodos
parser code
{:
    public static Nodo raiz;
    public static LinkedList<TError> listaErrores = new LinkedList<TError>(); 

    //Metodo al que se llama automaticamente ante algun error sintactico
    public void syntax_error(Symbol s)
    {
        System.out.print("\n[Recuperado]"); 
        
        String lexema = s.value.toString();
        int fila = s.right + 1;
        int columna = s.left + 1;
        
        TError datos = new TError(1,lexema,fila,columna);
        listaErrores.add(datos); 
    }

    //Metodo al que se llama en el momento en que ya no es posible una recuperacion de errores
    public void unrecovered_syntax_error(Symbol s) throws java.lang.Exception
    {
        System.out.println("\t[Panic Mode!]"); 
    }
:}

//------> Codigo para las acciones gramaticales
action code
{:
:}

//---------------------------------------------------------------------
//-------------------------    Declarar terminales
//---------------------------------------------------------------------

terminal    tk_bool,tk_id,tk_numero,tk_cadena,tk_asignar,tk_cad_comillas;
terminal    corch_op,corch_cl,tk_ptcoma,tk_coma,parent_op,parent_cl; //tk_dospts,tk_igual;
terminal    tk_mas,tk_menos,tk_por,tk_div;
terminal    pr_grupo,pr_id;
terminal    pr_alineado,pr_formato,pr_letra,pr_tamtex,pr_texto,pr_fondoelem,pr_visible,
            pr_borde,pr_opaque,pr_colortext,pr_autored;
terminal    pr_izquierda,pr_derecha,pr_centrado,pr_justificado,pr_negrilla,pr_cursiva,pr_mayuscula,
            pr_minuscula,pr_capitalt,pr_horizontal,pr_vertical;

//---------------------------------------------------------------------
//-------------------------    Declarar no terminales
//---------------------------------------------------------------------

non terminal Nodo INICIO, INICIO_LT, BLOQUE, EDITAR, LT_ELEMENTOS, ELEMENTO;
non terminal Nodo TIPO_DATO, VALOR_DATO, OPA;
non terminal Nodo ALINEADO, TEXTO, LETRA, TAMTEX, FONDO_ELEMENTO,
                  AUTO_REDIM, VISIBLE, BORDE, OPAQUE, COLOR_TEXT;
non terminal Nodo FORMATO, FORMATO_LT, AUTO_REDIM_VAL, BORDE_VAL;
non terminal String FORMATO_VAL, ALINEADO_VAL;

//---------------------------------------------------------------------
//-------------------------    Precedencia - de menor a mayor
//---------------------------------------------------------------------

precedence left tk_mas, tk_menos;
precedence left tk_por, tk_div;

//---------------------------------------------------------------------
//-------------------------    Inicio Gramatica
//---------------------------------------------------------------------

start with INICIO;

INICIO::= INICIO_LT:raiz
            {: 
                parser.raiz = raiz;
            :}
        ;

INICIO_LT::= INICIO_LT:lista BLOQUE:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | INICIO_LT:lista ELEMENTO:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | BLOQUE:h1 {: RESULT = new Nodo("INICIO_LT",h1); :}
        | ELEMENTO:h1 {: RESULT = new Nodo("INICIO_LT",h1); :}
        | error tk_ptcoma
        ;

BLOQUE::= tk_id:txt corch_op LT_ELEMENTOS:cont corch_cl 
            {:
                RESULT = new Nodo("BLOQUE",txt.toString(),cont);
            :}
        ;

EDITAR::= pr_grupo parent_op tk_id:txt parent_cl tk_ptcoma
        {:
            RESULT = new Nodo("EDITAR",txt.toString(),"pr_grupo");
        :}
    | pr_id parent_op tk_id:txt parent_cl tk_ptcoma
        {:
            RESULT = new Nodo("EDITAR",txt.toString(),"pr_id");
        :}
    ;

LT_ELEMENTOS::= LT_ELEMENTOS:lista ELEMENTO:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ELEMENTO:h1 {: RESULT = new Nodo("LT_ELEMENTOS",h1); :}
        | error tk_ptcoma
        ;
ELEMENTO::= EDITAR:nd {: RESULT = nd; :}
        | ALINEADO:nd {: RESULT = nd; :}
        | TEXTO:nd {: RESULT = nd; :}
        | FORMATO:nd {: RESULT = nd; :}
        | LETRA:nd {: RESULT = nd; :}
        | TAMTEX:nd {: RESULT = nd; :}
        | FONDO_ELEMENTO:nd {: RESULT = nd; :}
        | AUTO_REDIM:nd {: RESULT = nd; :}
        | VISIBLE:nd {: RESULT = nd; :}
        | BORDE:nd {: RESULT = nd; :}
        | OPAQUE:nd {: RESULT = nd; :}
        | COLOR_TEXT:nd {: RESULT = nd; :}
        ;

//---------------------------------------------------------------------
//-------------------------    Operaciones Aritmeticas
//---------------------------------------------------------------------

TIPO_DATO::= tk_numero:txt {: RESULT = new Nodo("TIPO_DATO",txt.toString()); :}
        | tk_cadena:txt {: RESULT = new Nodo("TIPO_DATO",txt.toString()); :}
        | tk_bool:txt {: RESULT = new Nodo("TIPO_DATO",txt.toString()); :}
        ;

VALOR_DATO::= OPA:nd {: RESULT = nd; :} ;
OPA::= OPA:h1 tk_mas OPA:h2
            {: 
                Nodo nd = new Nodo("OPA","tk_mas");
                nd.listaHijos.add(h1);
                nd.listaHijos.add(h2);
                RESULT = nd;
            :}
        | OPA:h1 tk_menos OPA:h2
            {: 
                Nodo nd = new Nodo("OPA","tk_menos");
                nd.listaHijos.add(h1);
                nd.listaHijos.add(h2);
                RESULT = nd;
            :}
        | OPA:h1 tk_por OPA:h2
            {: 
                Nodo nd = new Nodo("OPA","tk_por");
                nd.listaHijos.add(h1);
                nd.listaHijos.add(h2);
                RESULT = nd;
            :}
        | OPA:h1 tk_div OPA:h2 
            {: 
                Nodo nd = new Nodo("OPA","tk_div");
                nd.listaHijos.add(h1);
                nd.listaHijos.add(h2);
                RESULT = nd;
            :}
        | parent_op OPA:h1 parent_cl {: RESULT = h1; :}
        | TIPO_DATO:h1 {: RESULT = h1; :}
        | error OPA
        ;

//---------------------------------------------------------------------
//-------------------------    Definicion de elementos
//---------------------------------------------------------------------

ALINEADO::= pr_alineado tk_asignar ALINEADO_VAL:txt tk_ptcoma 
            {:
                RESULT = new Nodo("ALINEADO",txt);
            :}
        ;
ALINEADO_VAL::= pr_izquierda:txt {: RESULT = txt.toString(); :}
        | pr_derecha:txt {: RESULT = txt.toString(); :}
        | pr_centrado:txt {: RESULT = txt.toString(); :}
        | pr_justificado:txt {: RESULT = txt.toString(); :}
        | error tk_ptcoma
        ;

TEXTO::= pr_texto tk_asignar VALOR_DATO:val tk_ptcoma
            {:
                RESULT = new Nodo("TEXTO",val);
            :}
        ;

FORMATO::= pr_formato tk_asignar FORMATO_LT:cont tk_ptcoma 
            {:
                RESULT = new Nodo("FORMATO",cont);
            :}
        ;
FORMATO_LT::= FORMATO_LT:lista tk_coma FORMATO_VAL:txt
            {:
                lista.listaValores.add(txt);
                RESULT = lista;
            :}
        | FORMATO_VAL:txt 
            {:
                Nodo nd = new Nodo("FORMATO_LT");
                nd.listaValores.add(txt);
                RESULT = nd;
            :}
        | error tk_coma
        | error tk_ptcoma
        ;
FORMATO_VAL::= pr_negrilla:txt {: RESULT = txt.toString(); :}
        | pr_cursiva:txt {: RESULT = txt.toString(); :}
        | pr_mayuscula:txt {: RESULT = txt.toString(); :}
        | pr_minuscula:txt {: RESULT = txt.toString(); :}
        | pr_capitalt:txt {: RESULT = txt.toString(); :}
        ;

LETRA::= pr_letra tk_asignar VALOR_DATO:val tk_ptcoma
            {: 
                RESULT = new Nodo("LETRA",val); 
            :}
        | pr_letra tk_asignar tk_cad_comillas:txt tk_ptcoma
            {: 
                RESULT = new Nodo("LETRA",txt.toString()); 
            :}
        ;

TAMTEX::= pr_tamtex tk_asignar tk_numero:txt tk_ptcoma
            {: 
                RESULT = new Nodo("TAMTEX",txt.toString()); 
            :}
        ;

FONDO_ELEMENTO::= pr_fondoelem tk_asignar tk_cadena:txt tk_ptcoma 
            {: 
                RESULT = new Nodo("FONDO_ELEMENTO",txt.toString()); 
            :}
        ;

AUTO_REDIM::= pr_autored tk_asignar corch_op AUTO_REDIM_VAL:lista corch_cl tk_ptcoma
            {: 
                RESULT = new Nodo("AUTO_REDIM",lista); 
            :}
        ;
AUTO_REDIM_VAL::= tk_bool:val tk_coma pr_horizontal
            {:
                Nodo nd = new Nodo("AUTO_REDIM_VAL");
                nd.listaValores.add(val.toString());
                nd.listaValores.add("pr_horizontal");
                RESULT = nd;
            :}
        | tk_bool:val tk_coma pr_vertical
            {:
                Nodo nd = new Nodo("AUTO_REDIM_VAL");
                nd.listaValores.add(val.toString());
                nd.listaValores.add("pr_vertical");
                RESULT = nd;
            :}
        | error tk_ptcoma
        ;

VISIBLE::= pr_visible tk_asignar tk_bool:txt tk_ptcoma
            {:
                RESULT = new Nodo("VISIBLE",txt.toString());
            :}
        ;

BORDE::= pr_borde tk_asignar corch_op BORDE_VAL:cont corch_cl tk_ptcoma
            {:
                RESULT = new Nodo("BORDE",cont);
            :}
        ;
BORDE_VAL::= tk_numero:val1 tk_coma tk_cadena:val2 tk_coma tk_bool:val3
            {:
                Nodo nd = new Nodo("BORDE_VAL");
                nd.listaValores.add(val1.toString());
                nd.listaValores.add(val2.toString());
                nd.listaValores.add(val3.toString());
                RESULT = nd;
            :}
        ;

OPAQUE::= pr_opaque tk_asignar tk_bool:txt tk_ptcoma
            {:
                RESULT = new Nodo("OPAQUE",txt.toString());
            :}
        ;

COLOR_TEXT::= pr_colortext tk_asignar tk_cadena:txt tk_ptcoma
            {:
                RESULT = new Nodo("COLOR_TEXT",txt.toString());
            :}
        ;
