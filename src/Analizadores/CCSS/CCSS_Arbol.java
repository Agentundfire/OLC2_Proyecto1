package Analizadores.CCSS;

import olc2_proyecto1.Nodo;

public class CCSS_Arbol {

    public CCSS_Arbol() {
    }
    
    public static void AccionesCCSS(){
        Nodo raiz = CCSS_Sintactico.raiz;
        if (raiz == null) { System.out.println("Raiz is null"); return; }
        ACCION("RAIZ",raiz);
    }
    
    private static void RecorrerHijos(Nodo padre){
        for (Nodo subEtiqueta : padre.listaHijos) {
            ACCION(padre.TAG,subEtiqueta);
        }
    }
    
    private static void ACCION(String padre, Nodo etiqueta){
        if(etiqueta == null) return;
        
        switch(etiqueta.TAG){
            case "INICIO_LT":
            case "BLOQUE":
            case "EDITAR":
            case "LT_ELEMENTOS":
            case "TIPO_DATO":
            case "OPA":
            case "ALINEADO":
            case "TEXTO":
            case "FORMATO":
            case "FORMATO_LT":
            case "LETRA":
            case "TAMTEX":
            case "FONDO_ELEMENTO":
            case "AUTO_REDIM":
            case "AUTO_REDIM_VAL":
            case "VISIBLE":
            case "BORDE":
            case "BORDE_VAL":
            case "OPAQUE":
            case "COLOR_TEXT":
                    System.out.println("TAG " + padre + "-" + etiqueta.TAG + " -> h[" 
                            + etiqueta.listaHijos.size() + "]");
                    RecorrerHijos(etiqueta);
                    break;
            default:
                    System.out.println("\tDEFAULT " + padre + "-" + etiqueta.TAG + " -> h[" 
                            + etiqueta.listaHijos.size() + "]");
                    RecorrerHijos(etiqueta);
                break;
        }
    }
    
}
