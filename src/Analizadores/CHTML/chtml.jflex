/*------------  1ra Area: Codigo de Usuario ---------*/
//------> *074Paquetes,importaciones
package Analizadores.CHTML;
import olc2_proyecto1.TError;
import java_cup.runtime.*;
import java.util.LinkedList;

/*------------  2da Area: Opciones y Declaraciones ---------*/
%%
%{
    //----> Codigo de usuario en sintaxis java
    public static LinkedList<TError> listaErrores = new LinkedList<TError>();
%}

//-------> Directivas
%public 
%class CHTML_Lexico
%cupsym CHTML_Simbolos
%cup
%char
%column
%full
%ignorecase
%line
%unicode

/* Espacio blanco */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

/* Comentarios */
Comment = "<//-" ~"-//>"

/* ER */
ID = [:jletter:] ([:jletterdigit:]|{punctuation})*
numero = [0-9]+ ("."[0-9]+)?
cadena = "\"" ~"\""
//texto =  ">" ~"<"
punctuation = [-/_.,~!@#%\^&*|(){}\[\]?+\\:`]

%%

/* Palabras reservadas */
<YYINITIAL> "CHTML"             { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_chtml, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CHTML>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_chtml, yycolumn, yyline, yytext()); }
<YYINITIAL> "ENCABEZADO"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_encabezado, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-ENCABEZADO>"    { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_encabezado, yycolumn, yyline, yytext()); }
<YYINITIAL> "CJS"               { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_cjs, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CJS>"           { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_cjs, yycolumn, yyline, yytext()); }
<YYINITIAL> "CCSS"              { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_ccss, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CCSS>"          { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_ccss, yycolumn, yyline, yytext()); }
<YYINITIAL> "CUERPO"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_cuerpo, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CUERPO>"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_cuerpo, yycolumn, yyline, yytext()); }
<YYINITIAL> "TITULO"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_titulo, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-TITULO>"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_titulo, yycolumn, yyline, yytext()); }
<YYINITIAL> "PANEL"             { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_panel, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-PANEL>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_panel, yycolumn, yyline, yytext()); }
<YYINITIAL> "TEXTO"             { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_texto, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-TEXTO>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_texto, yycolumn, yyline, yytext()); }
<YYINITIAL> "IMAGEN"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_imagen, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-IMAGEN>"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_imagen, yycolumn, yyline, yytext()); }
<YYINITIAL> "BOTON"             { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_boton, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-BOTON>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_boton, yycolumn, yyline, yytext()); }
<YYINITIAL> "ENLACE"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_enlace, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-ENLACE>"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_enlace, yycolumn, yyline, yytext()); }
<YYINITIAL> "TABLA"             { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_tabla, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-TABLA>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_tabla, yycolumn, yyline, yytext()); }
<YYINITIAL> "FIL_T"             { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_filt, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-FIL_T>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_filt, yycolumn, yyline, yytext()); }
<YYINITIAL> "CB"                { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_cb, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CB>"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_cb, yycolumn, yyline, yytext()); }
<YYINITIAL> "CT"                { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_ct, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CT>"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_ct, yycolumn, yyline, yytext()); }
<YYINITIAL> "TEXTO_A"           { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_texto_a, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-TEXTO_A>"       { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_texto_a, yycolumn, yyline, yytext()); }
<YYINITIAL> "CAJA_TEXTO"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_caja_texto, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CAJA_TEXTO>"    { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_caja_texto, yycolumn, yyline, yytext()); }
<YYINITIAL> "CAJA"              { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_caja, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-CAJA>"          { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_caja, yycolumn, yyline, yytext()); }
<YYINITIAL> "OPCION"            { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_opcion, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-OPCION>"        { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_opcion, yycolumn, yyline, yytext()); }
<YYINITIAL> "SPINNER"           { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.op_spinner, yycolumn, yyline, yytext()); }
<YYINITIAL> "<FIN-SPINNER>"       { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_spinner, yycolumn, yyline, yytext()); }
<YYINITIAL> "<SALTO-FIN>"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.cl_salto, yycolumn, yyline, yytext()); }

<YYINITIAL> "fondo"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_fondo , yycolumn, yyline, yytext()); }
<YYINITIAL> "ruta"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_ruta , yycolumn, yyline, yytext()); }
<YYINITIAL> "click"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_click , yycolumn, yyline, yytext()); }
<YYINITIAL> "valor"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_valor , yycolumn, yyline, yytext()); }
<YYINITIAL> "Id"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_id , yycolumn, yyline, yytext()); }
<YYINITIAL> "Grupo"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_grupo , yycolumn, yyline, yytext()); }
<YYINITIAL> "Alto"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_alto , yycolumn, yyline, yytext()); }
<YYINITIAL> "Ancho"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_ancho , yycolumn, yyline, yytext()); }
<YYINITIAL> "Alineado"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_alineado , yycolumn, yyline, yytext()); }
//<YYINITIAL> "ccss"         { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.pr_ccss , yycolumn, yyline, yytext()); }

<YYINITIAL> {
    /* ER */    
    {numero}    { System.out.println("Reconocio numero "+yytext()); return new Symbol(CHTML_Simbolos.tk_numero, yycolumn, yyline, yytext()); }
    {cadena}    { System.out.println("Reconocio cadena "+yytext()); return new Symbol(CHTML_Simbolos.tk_cadena, yycolumn, yyline, yytext()); }
    {punctuation} { System.out.println("Reconocio punctuation "+yytext()); return new Symbol(CHTML_Simbolos.tk_punct, yycolumn, yyline, yytext()); }
    {ID}        { System.out.println("Reconocio ID "+yytext()); return new Symbol(CHTML_Simbolos.tk_id, yycolumn, yyline, yytext()); }

    /* caracteres especiales */
    "<"     { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.EtOp , yycolumn, yyline, yytext()); }
    ">"     { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.EtCl , yycolumn, yyline, yytext()); }
    "="     { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.tk_igual, yycolumn, yyline, yytext()); }
    ";"     { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.tk_ptcoma, yycolumn, yyline, yytext()); }
//    "("     { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.parent_op , yycolumn, yyline, yytext()); }
//    ")"     { System.out.println("Reconocio "+yytext()); return new Symbol(CHTML_Simbolos.parent_cl , yycolumn, yyline, yytext()); }

    /* Espacio en blanco */
      {WhiteSpace}      { /* ignore */ }
    /* Comentarios */
      {Comment}         { System.out.println("Reconocio comentario: " + yytext()); /* ignore */ }
    }


/* error fallback*/
[^]                       { TError datos = new TError(0,yytext(),yyline,yycolumn); listaErrores.add(datos); }