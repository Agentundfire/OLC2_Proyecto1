package Generadores;

public class cjs_generarSintactico
{
   public static void main(String[] args)
    {
        String opciones[] = new String[7]; 
        
        //Le damos la dirección, carpeta donde se vaN a generar los archivos
        opciones[0] = "-destdir";
        opciones[1] = "src/Analizadores/CJS/";
        
        //Seleccionamos la opción y nombre del archivo simbolos
        opciones[2] = "-symbols"; 
        opciones[3] = "CJS_Simbolos";
        
        //Seleccionamos la opcion y nombre de la clase parser
        opciones[4] = "-parser";         
        opciones[5] = "CJS_Sintactico"; 
        
        //Le decimos donde se encuentra el archivo .cup 
        opciones[6] = "src/Analizadores/CJS/cjs.cup"; 
        try 
        {
            java_cup.Main.main(opciones);
        } 
        catch (Exception ex)
        {
            System.out.print(ex);
        }
    }
}
