package Generadores;
import java.io.File;

public class ccss_generarLexico 
{
    public static void main(String[] args) 
    {
        String path="src/Analizadores/CCSS/ccss.jflex";
        generarLexico(path);
    } 
    
    public static void generarLexico(String path)
    {
        File file=new File(path);
        jflex.Main.generate(file);
    } 
}
