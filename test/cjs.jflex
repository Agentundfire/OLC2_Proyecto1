package Analizadores.CJS;
import olc2_proyecto1.TError;
import java_cup.runtime.*;
import java.util.LinkedList;

%%

%{
    public static LinkedList<TError> listaErrores = new LinkedList<TError>();
%}

%public 
%class CJS_Lexico
%cupsym CJS_Simbolos
%cup
%char
%column
%full
%ignorecase
%line
%unicode

/* Espacio blanco */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n\']
WhiteSpace     = {LineTerminator} | [ \t\f]

/* Comentarios */
Comentario_Multilinea = "\'/" [^\'] ~"/\'" | "\'/" "/"+ "\'"
Comentario_simple = "\'" {InputCharacter}* {LineTerminator}?
Comment = {Comentario_Multilinea} | {Comentario_simple}+

/* ER */
entero = [0-9]+
numero = {entero} ("."{entero}+)?
booleano = "true"|"false"
ID = [:jletter:] ([:jletterdigit:]|"_")*
texto = "\"" ~"\""
fecha = {entero} "/" {entero} "/" {entero}
tiempo = {entero} ":" {entero} ":" {entero}
tk_date = "\'" {fecha} "\'"
tk_datetime  = "\'" {fecha} " " {tiempo} "\'"


%%

/* Palabras reservadas */
<YYINITIAL> "DimV"			{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_dim, yycolumn, yyline, yytext()); }
<YYINITIAL> "Si"			{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_si, yycolumn, yyline, yytext()); }
<YYINITIAL> "Sino"			{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_sino, yycolumn, yyline, yytext()); }
<YYINITIAL> "Selecciona"	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_selecciona, yycolumn, yyline, yytext()); }
<YYINITIAL> "Caso"			{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_caso, yycolumn, yyline, yytext()); }
<YYINITIAL> "Defecto"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_defecto, yycolumn, yyline, yytext()); }
<YYINITIAL> "Para"			{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_para, yycolumn, yyline, yytext()); }
<YYINITIAL> "Mientras"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_mientras, yycolumn, yyline, yytext()); }
<YYINITIAL> "Detener"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_detener, yycolumn, yyline, yytext()); }
<YYINITIAL> "Funcion"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_funcion, yycolumn, yyline, yytext()); }
<YYINITIAL> "Retornar"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_retornar, yycolumn, yyline, yytext()); }
<YYINITIAL> "Imprimir"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_imprimir, yycolumn, yyline, yytext()); }
<YYINITIAL> "Mensaje"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_mensaje, yycolumn, yyline, yytext()); }

<YYINITIAL> "Documento"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_doc, yycolumn, yyline, yytext()); }
<YYINITIAL> "Obtener"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_obtener, yycolumn, yyline, yytext()); }
<YYINITIAL> "SetElemento"	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_setelemento, yycolumn, yyline, yytext()); }
<YYINITIAL> "Observador"	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.pr_observador, yycolumn, yyline, yytext()); }

<YYINITIAL> {
	/* ER */
	{numero}	{ System.out.println("Reconocio numero "+yytext()); return new Symbol(CJS_Simbolos.tk_numero, yycolumn, yyline, yytext()); }
	{booleano}	{ System.out.println("Reconocio booleano "+yytext()); return new Symbol(CJS_Simbolos.tk_booleano, yycolumn, yyline, yytext()); }
	{ID}		{ System.out.println("Reconocio ID "+yytext()); return new Symbol(CJS_Simbolos.tk_id, yycolumn, yyline, yytext()); }
	{texto}	{ System.out.println("Reconocio texto "+yytext()); return new Symbol(CJS_Simbolos.tk_texto, yycolumn, yyline, yytext()); }
	{tk_date}	{ System.out.println("Reconocio date "+yytext()); return new Symbol(CJS_Simbolos.tk_date, yycolumn, yyline, yytext()); }
	{tk_datetime}	{ System.out.println("Reconocio datetime "+yytext()); return new Symbol(CJS_Simbolos.tk_datetime, yycolumn, yyline, yytext()); }

    /* caracteres especiales */
    ";"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_ptcoma, yycolumn, yyline, yytext()); }
    ":"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_dospts, yycolumn, yyline, yytext()); }
    ","		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_coma, yycolumn, yyline, yytext()); }
    "."		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_punto, yycolumn, yyline, yytext()); }
    "("		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.parent_op, yycolumn, yyline, yytext()); }
    ")"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.parent_cl, yycolumn, yyline, yytext()); }
    "{"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.llave_op, yycolumn, yyline, yytext()); }
    "}"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.llave_cl, yycolumn, yyline, yytext()); }
    

    /* operadores */
    "++"	{ System.out.println("Reconocio ++ "+yytext()); return new Symbol(CJS_Simbolos.tk_adicion, yycolumn, yyline, yytext()); }
    "--"	{ System.out.println("Reconocio -- "+yytext()); return new Symbol(CJS_Simbolos.tk_sustraccion, yycolumn, yyline, yytext()); }
    "+"		{ System.out.println("Reconocio + "+yytext()); return new Symbol(CJS_Simbolos.tk_mas, yycolumn, yyline, yytext()); }
    "-"		{ System.out.println("Reconocio - "+yytext()); return new Symbol(CJS_Simbolos.tk_menos, yycolumn, yyline, yytext()); }
    "*"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_por, yycolumn, yyline, yytext()); }
    "/"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_div, yycolumn, yyline, yytext()); }
    "^"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_pot, yycolumn, yyline, yytext()); }
    "%"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_mod, yycolumn, yyline, yytext()); }
    
    "=="	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_igual, yycolumn, yyline, yytext()); }
    "!="	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_dif, yycolumn, yyline, yytext()); }
    "<="	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_menigual, yycolumn, yyline, yytext()); }
    ">="	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_mayigual, yycolumn, yyline, yytext()); }
    "<"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_menor, yycolumn, yyline, yytext()); }
    ">"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_mayor, yycolumn, yyline, yytext()); }
    
    "&&"	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_and, yycolumn, yyline, yytext()); }
    "||"	{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_or, yycolumn, yyline, yytext()); }
    "!"		{ System.out.println("Reconocio "+yytext()); return new Symbol(CJS_Simbolos.tk_not, yycolumn, yyline, yytext()); }

    /* Espacio en blanco */
      {WhiteSpace}		{ /* ignore */ }
    /* Comentarios */
      {Comment}			{ System.out.println("Reconocio comentario: " + yytext()); /* ignore */ }
    }


/* error fallback*/
[^]                       { TError datos = new TError(0,yytext(),yyline,yycolumn); listaErrores.add(datos); }
